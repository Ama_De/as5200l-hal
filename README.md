# AS5200L STM32 HAL I2C library

Library for STM32 HAL interfacing AS5200L magnetic encoder over I2C bus.
  
  
Example program provided, tested on Nucleo-L476RG.
  
Notes: 
- When setting start and stop angle, stop angle has to be minimum 18 degrees (205 steps) greater than start angle. Otherwise stop angle won't be setted up and it's MPOS register will respond with the same value as start angle ZPOS register.
  
  
GNU License  
Copyright (c) 2020, Amadeusz Świerk