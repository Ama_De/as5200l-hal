#include "AS5200L.h"

void AS5200L_init (AS5200L_HandleTypeDef *magnetic_encoder, I2C_HandleTypeDef *i2c, uint16_t address) {
	magnetic_encoder->i2cInstantion = i2c;
	magnetic_encoder->i2cAddress = address << 1;     // LSB is used for flag informing about reading or writing operation, adress is 7 bit
}

uint8_t getBit (uint8_t value, uint8_t bitNumber) {
	return (uint8_t)((value & (1 << bitNumber)) >> bitNumber);
}

HAL_StatusTypeDef AS5200L_getRawAngle (AS5200L_HandleTypeDef *encoder, uint16_t *angle) {
	uint8_t angleRead[2] = {0, 0};
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_outputRawAngleLSBAddress, 1, angleRead, 2, AS5200L_I2C_TIMEOUT);
	*angle = (status == HAL_OK ? angleRead[0] + (angleRead[1] & msbMask) * 256 : -1);
	return status;
}

HAL_StatusTypeDef AS5200L_isRawAngleFromFastFilter (AS5200L_HandleTypeDef *encoder, uint8_t *isFromFastFilter) {
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_outputRawAngleMSBAddress, 1, isFromFastFilter, 1, AS5200L_I2C_TIMEOUT);
	*isFromFastFilter = getBit(*isFromFastFilter, 7);
	return status;
}

HAL_StatusTypeDef AS5200L_getAngle (AS5200L_HandleTypeDef *encoder, uint16_t *angle) {
	uint8_t angleRead[2] = {0, 0};
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_outputAngleLSBAddress, 1, angleRead, 2, AS5200L_I2C_TIMEOUT);
	*angle = (status == HAL_OK ? angleRead[0] + (angleRead[1] & msbMask) * 256 * 10 : -1);
	return status;
}

HAL_StatusTypeDef AS5200L_isSlowFilterBusy (AS5200L_HandleTypeDef *encoder, uint8_t *isBusy) {
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_outputRawAngleMSBAddress, 1, isBusy, 1, AS5200L_I2C_TIMEOUT);
	*isBusy = getBit(*isBusy, 6);
	return status;
}

HAL_StatusTypeDef AS5200L_getStartPosition (AS5200L_HandleTypeDef *encoder, uint16_t *angle) {
	uint8_t angleRead[2] = {0, 0};
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationZposMSBAddress, 1, angleRead, 2, AS5200L_I2C_TIMEOUT);
	*angle = (status == HAL_OK ? (angleRead[1] + (angleRead[0] & msbMask) * 256) : -1);
	return status;
}

HAL_StatusTypeDef AS5200L_setStartPosition (AS5200L_HandleTypeDef *encoder, uint16_t angle) {
	if (angle <= 4096) {
		uint8_t _lsb = (uint16_t)angle & 0xff;
		uint8_t _msb = (uint16_t)angle >> 8;
		uint8_t *writeAngle[2] = {&_msb, &_lsb};
		return HAL_I2C_Mem_Write(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationZposMSBAddress, 1, *writeAngle, 2, AS5200L_I2C_TIMEOUT);
	}
	return HAL_ERROR;
}

HAL_StatusTypeDef AS5200L_getStopPosition (AS5200L_HandleTypeDef *encoder, uint16_t *angle) {
	uint8_t angleRead[2] = {0, 0};
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationMposMSBAddress, 1, angleRead, 2, AS5200L_I2C_TIMEOUT);
	*angle = (status == HAL_OK ? (angleRead[1] + (angleRead[0] & msbMask) * 256) : -1);
	return status;
}

HAL_StatusTypeDef AS5200L_setStopPosition (AS5200L_HandleTypeDef *encoder, uint16_t angle) {
	uint16_t startAngle = 0;
	AS5200L_getStartPosition(encoder, &startAngle);

	if (angle <= 4096 && ((angle - startAngle) >= 205)) {
		uint8_t _lsb = (uint16_t)angle & 0xff;
		uint8_t _msb = (uint16_t)angle >> 8;
		uint8_t *writeAngle[2] = {&_msb, &_lsb};
		return HAL_I2C_Mem_Write(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationMposMSBAddress, 1, *writeAngle, 2, AS5200L_I2C_TIMEOUT);
	}
	return HAL_ERROR;

}

HAL_StatusTypeDef AS5200L_setPowerMode (AS5200L_HandleTypeDef *encoder, uint8_t mode) {
	uint8_t configurationLSB = 0;
	HAL_StatusTypeDef statusRead = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfLSBAddress, 1, &configurationLSB, 1, AS5200L_I2C_TIMEOUT);
	switch (mode) {
		default:
			configurationLSB &= ~(1 << 0);
			configurationLSB &= ~(1 << 1);
			break;

		case AS5200L_power_mode_lpm1:
			configurationLSB |= (1 << 0);
			configurationLSB &= ~(1 << 1);
			break;

		case AS5200L_power_mode_lpm2:
			configurationLSB &= (1 << 0);
			configurationLSB |= (1 << 1);
			break;

		case AS5200L_power_mode_lpm3:
			configurationLSB |= (1 << 0);
			configurationLSB |= (1 << 1);
			break;
	}

	HAL_StatusTypeDef statusWrite = HAL_I2C_Mem_Write(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfLSBAddress, 1, &configurationLSB, 1, AS5200L_I2C_TIMEOUT);

	if (statusRead != HAL_OK || statusWrite != HAL_OK) {
		return HAL_ERROR;
	} else {
		return HAL_OK;
	}
}

HAL_StatusTypeDef AS5200L_setHysteresis (AS5200L_HandleTypeDef *encoder, uint8_t mode) {
	uint8_t configurationLSB = 0;
	HAL_StatusTypeDef statusRead = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfLSBAddress, 1, &configurationLSB, 1, AS5200L_I2C_TIMEOUT);

	switch (mode) {
		default:
			configurationLSB &= ~(1 << 2);
			configurationLSB &= ~(1 << 3);
			break;

		case AS5200L_hysteresis_lsbx1:
			configurationLSB |= (1 << 2);
			configurationLSB &= ~(1 << 3);
			break;

		case AS5200L_hysteresis_lsbx2:
			configurationLSB &= ~(1 << 2);
			configurationLSB |= (1 << 3);
			break;

		case AS5200L_hysteresis_lsbx3:
			configurationLSB |= (1 << 2);
			configurationLSB |= (1 << 3);
			break;
	}

	HAL_StatusTypeDef statusWrite = HAL_I2C_Mem_Write(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfLSBAddress, 1, &configurationLSB, 1, AS5200L_I2C_TIMEOUT);

	if (statusRead != HAL_OK || statusWrite != HAL_OK) {
		return HAL_ERROR;
	} else {
		return HAL_OK;
	}
}

HAL_StatusTypeDef AS5200L_setPWMOutput (AS5200L_HandleTypeDef *encoder, uint16_t pwmFrequency) {
	uint8_t configurationLSB = 0;
	HAL_StatusTypeDef statusRead = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfLSBAddress, 1, &configurationLSB, 1, AS5200L_I2C_TIMEOUT);

	switch (pwmFrequency) {
		default:
			configurationLSB &= ~(1 << 5);
			configurationLSB &= ~(1 << 6);
			configurationLSB &= ~(1 << 7);
			break;

		case 115:
			configurationLSB |= (1 << 5);
			configurationLSB &= ~(1 << 6);
			configurationLSB &= ~(1 << 7);
			break;

		case 230:
			configurationLSB |= (1 << 5);
			configurationLSB |= (1 << 6);
			configurationLSB &= ~(1 << 7);
			break;

		case 460:
			configurationLSB |= (1 << 5);
			configurationLSB &= ~(1 << 6);
			configurationLSB |= (1 << 7);
			break;

		case 920:
			configurationLSB |= (1 << 5);
			configurationLSB |= (1 << 6);
			configurationLSB |= (1 << 7);
			break;
	}

	HAL_StatusTypeDef statusWrite = HAL_I2C_Mem_Write(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfLSBAddress, 1, &configurationLSB, 1, AS5200L_I2C_TIMEOUT);

	if (statusRead != HAL_OK || statusWrite != HAL_OK) {
		return HAL_ERROR;
	} else {
		return HAL_OK;
	}
}

HAL_StatusTypeDef AS5200L_setSlowFilter (AS5200L_HandleTypeDef *encoder, uint8_t mode) {
	uint8_t configurationMSB = 0;
	HAL_StatusTypeDef statusRead = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfMSBAddress, 1, &configurationMSB, 1, AS5200L_I2C_TIMEOUT);

	switch (mode) {
		default:
			configurationMSB &= ~(1 << 0);
			configurationMSB &= ~(1 << 1);
			break;

		case AS5200L_slow_filter_lsbx8:
			configurationMSB |= (1 << 0);
			configurationMSB &= ~(1 << 1);
			break;

		case AS5200L_slow_filter_lsbx4:
			configurationMSB &= ~(1 << 0);
			configurationMSB |= (1 << 1);
			break;

		case AS5200L_slow_filter_lsbx2:
			configurationMSB |= (1 << 0);
			configurationMSB |= (1 << 1);
			break;
	}

	HAL_StatusTypeDef statusWrite = HAL_I2C_Mem_Write(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfMSBAddress, 1, &configurationMSB, 1, AS5200L_I2C_TIMEOUT);

	if (statusRead != HAL_OK || statusWrite != HAL_OK) {
		return HAL_ERROR;
	} else {
		return HAL_OK;
	}
}

HAL_StatusTypeDef AS5200L_setFastFilter (AS5200L_HandleTypeDef *encoder, uint8_t mode) {
	uint8_t configurationMSB = 0;
	HAL_StatusTypeDef statusRead = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfMSBAddress, 1, &configurationMSB, 1, AS5200L_I2C_TIMEOUT);

	switch (mode) {
		default:
			configurationMSB &= ~(1 << 2);
			configurationMSB &= ~(1 << 3);
			configurationMSB &= ~(1 << 4);
			break;

		case AS5200L_fast_filter_lsbx6:
			configurationMSB |= (1 << 2);
			configurationMSB &= ~(1 << 3);
			configurationMSB &= ~(1 << 4);
			break;

		case AS5200L_fast_filter_lsbx7:
			configurationMSB &= ~(1 << 2);
			configurationMSB |= (1 << 3);
			configurationMSB &= ~(1 << 4);
			break;

		case AS5200L_fast_filter_lsbx9:
			configurationMSB |= (1 << 2);
			configurationMSB |= (1 << 3);
			configurationMSB &= ~(1 << 4);
			break;

		case AS5200L_fast_filter_lsbx18:
			configurationMSB &= ~(1 << 2);
			configurationMSB &= ~(1 << 3);
			configurationMSB |= (1 << 4);
			break;

		case AS5200L_fast_filter_lsbx21:
			configurationMSB |= (1 << 2);
			configurationMSB &= ~(1 << 3);
			configurationMSB |= (1 << 4);
			break;

		case AS5200L_fast_filter_lsbx24:
			configurationMSB &= ~(1 << 2);
			configurationMSB |= (1 << 3);
			configurationMSB |= (1 << 4);
			break;

		case AS5200L_fast_filter_lsbx10:
			configurationMSB |= (1 << 2);
			configurationMSB |= (1 << 3);
			configurationMSB |= (1 << 4);
			break;
	}

	HAL_StatusTypeDef statusWrite = HAL_I2C_Mem_Write(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfMSBAddress, 1, &configurationMSB, 1, AS5200L_I2C_TIMEOUT);

	if (statusRead != HAL_OK || statusWrite != HAL_OK) {
		return HAL_ERROR;
	} else {
		return HAL_OK;
	}
}

HAL_StatusTypeDef AS5200L_setLPMTimer (AS5200L_HandleTypeDef *encoder, uint8_t value) {
	uint8_t configurationMSB = 0;
	HAL_StatusTypeDef statusRead = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfMSBAddress, 1, &configurationMSB, 1, AS5200L_I2C_TIMEOUT);
	if (value) {
		configurationMSB |= (1 << 5);
	} else {
		configurationMSB &= ~(1 << 5);
	}
	HAL_StatusTypeDef statusWrite = HAL_I2C_Mem_Write(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_configurationConfMSBAddress, 1, &configurationMSB, 1, AS5200L_I2C_TIMEOUT);

	if (statusRead != HAL_OK || statusWrite != HAL_OK) {
		return HAL_ERROR;
	} else {
		return HAL_OK;
	}
}

HAL_StatusTypeDef AS5200L_getMagnetStatus (AS5200L_HandleTypeDef *encoder, uint8_t *magnetStatus) {
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_statusStatusAddress, 1, magnetStatus, 1, AS5200L_I2C_TIMEOUT);
	uint8_t MH = getBit(*magnetStatus, 3), ML = getBit(*magnetStatus, 4), MD = getBit(*magnetStatus, 5);

	if (!MD) {
		*magnetStatus = AS5200L_magnet_notDetected;
	} else if (ML) {
		*magnetStatus = AS5200L_magnet_tooWeak;
	} else if (MH) {
		*magnetStatus = AS5200L_magnet_tooStrong;
	} else {
		*magnetStatus = AS5200L_magnet_correct;
	}
	return status;
}

HAL_StatusTypeDef AS5200L_getGain (AS5200L_HandleTypeDef *encoder, uint8_t *gain) {
	return HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_statusGainAddress, 1, gain, 1, AS5200L_I2C_TIMEOUT);
}

HAL_StatusTypeDef AS5200L_getMagnitude (AS5200L_HandleTypeDef *encoder, uint16_t *magnitude) {
	uint8_t statusRead[2] = {0, 0};
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_statusMagnitudeMSBAddress, 1, statusRead, 2, AS5200L_I2C_TIMEOUT);
	*magnitude = (status == HAL_OK ? statusRead[1] + (statusRead[0] & msbMask) * 256 : -1);
		return status;
	}

	HAL_StatusTypeDef AS5200L_isSensorOverflowed (AS5200L_HandleTypeDef *encoder, uint8_t *isOverflowed) {
		HAL_StatusTypeDef status = HAL_I2C_Mem_Read(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_statusGainAddress, 1, isOverflowed, 1, AS5200L_I2C_TIMEOUT);
		*isOverflowed = getBit(*isOverflowed, 6);
		return status;
	}

	HAL_StatusTypeDef AS5200L_burnAngle (AS5200L_HandleTypeDef *encoder) {
		uint8_t magnetStatus = AS5200L_magnet_notDetected;
		AS5200L_getMagnetStatus(encoder, &magnetStatus);
		if (magnetStatus == AS5200L_magnet_correct) {
			return HAL_I2C_Mem_Write(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_burnAngleValue, 1, (uint8_t*)0xff, 1, AS5200L_I2C_TIMEOUT);
		}
		return HAL_ERROR;
	}

	HAL_StatusTypeDef AS5200L_burnSettings (AS5200L_HandleTypeDef *encoder) {
		return HAL_I2C_Mem_Write(encoder->i2cInstantion, encoder->i2cAddress, AS5200L_burnSettingsValue, 1, (uint8_t*)0xff, 1, AS5200L_I2C_TIMEOUT);
	}
