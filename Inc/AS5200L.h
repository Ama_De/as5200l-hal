/*
 AS5200L.h - Library for handling AS5200L magnetic encoder over I2C bus.
 Created by Amadeusz Świerk, 28.03.2020
 Released into the public domain, MIT license.
 */

#ifndef AS5200L_h
#define AS5200L_h

#include "stm32l4xx_hal.h"
#include "stdio.h"

#define AS5200L_I2C_TIMEOUT 10

#define AS5200L_bottomSensorAddress 0x40
#define AS5200L_topSensorAddress 0x41

// power mode
#define AS5200L_power_mode_normal 0
#define AS5200L_power_mode_lpm1 1
#define AS5200L_power_mode_lpm2 2
#define AS5200L_power_mode_lpm3 3

// hysteresis
#define AS5200L_hysteresis_off 0
#define AS5200L_hysteresis_lsbx1 1
#define AS5200L_hysteresis_lsbx2 2
#define AS5200L_hysteresis_lsbx3 3

// slow filter
#define AS5200L_slow_filter_lsbx16 0
#define AS5200L_slow_filter_lsbx8 1
#define AS5200L_slow_filter_lsbx4 2
#define AS5200L_slow_filter_lsbx2 3

// fast filter
#define AS5200L_fast_filter_off 0
#define AS5200L_fast_filter_lsbx6 1
#define AS5200L_fast_filter_lsbx7 2
#define AS5200L_fast_filter_lsbx9 3
#define AS5200L_fast_filter_lsbx18 4
#define AS5200L_fast_filter_lsbx21 5
#define AS5200L_fast_filter_lsbx24 6
#define AS5200L_fast_filter_lsbx10 7

// magnetState
#define AS5200L_magnet_notDetected 0
#define AS5200L_magnet_tooWeak 1
#define AS5200L_magnet_tooStrong 2
#define AS5200L_magnet_correct 3

// registers addresses
#define AS5200L_configurationZposMSBAddress 0x01
#define AS5200L_configurationMposMSBAddress 0x03

#define AS5200L_configurationConfMSBAddress 0x07
#define AS5200L_configurationConfLSBAddress 0x08

#define AS5200L_outputRawAngleLSBAddress 0x0D
#define AS5200L_outputRawAngleMSBAddress 0x0C
#define AS5200L_outputAngleLSBAddress 0x0F

#define AS5200L_statusStatusAddress 0x0B
#define AS5200L_statusGainAddress 0x1A
#define AS5200L_statusMagnitudeMSBAddress 0x1B

#define AS5200L_burnAdress 0xFF
#define AS5200L_burnAngleValue 0x80
#define AS5200L_burnSettingsValue 0x40

#define msbMask 0b00001111

typedef struct AS5200L {
	I2C_HandleTypeDef *i2cInstantion;
	uint16_t i2cAddress;
} AS5200L_HandleTypeDef;

void AS5200L_init(AS5200L_HandleTypeDef *magnetic_encoder, I2C_HandleTypeDef *i2c, uint16_t address);

// Output registers
HAL_StatusTypeDef AS5200L_getRawAngle(AS5200L_HandleTypeDef *encoder, uint16_t *angle);          // RAW ANGLE: RAW ANGLE
HAL_StatusTypeDef AS5200L_isRawAngleFromFastFilter(AS5200L_HandleTypeDef *encoder, uint8_t *isFromFastFilter); // RAW ANGLE: RAWFF
HAL_StatusTypeDef AS5200L_getAngle(AS5200L_HandleTypeDef *encoder, uint16_t *angle);             // ANGLE: ANGLE
HAL_StatusTypeDef AS5200L_isSlowFilterBusy(AS5200L_HandleTypeDef *encoder, uint8_t *isBusy);         // RAW ANGLE: SFBUSY

// Configuration registers
HAL_StatusTypeDef AS5200L_getStartPosition(AS5200L_HandleTypeDef *encoder, uint16_t *angle);   // ZPOS: ZPOS
HAL_StatusTypeDef AS5200L_setStartPosition(AS5200L_HandleTypeDef *encoder, uint16_t angle);    // ZPOS: ZPOS
HAL_StatusTypeDef AS5200L_getStopPosition(AS5200L_HandleTypeDef *encoder, uint16_t *angle);    // MPOS: MPOS
HAL_StatusTypeDef AS5200L_setStopPosition(AS5200L_HandleTypeDef *encoder, uint16_t angle);     // MPOS: MPOS
HAL_StatusTypeDef AS5200L_setPowerMode(AS5200L_HandleTypeDef *encoder, uint8_t mode);          // CONF: PM
HAL_StatusTypeDef AS5200L_setHysteresis(AS5200L_HandleTypeDef *encoder, uint8_t mode);         // CONF: HYST
HAL_StatusTypeDef AS5200L_setPWMOutput(AS5200L_HandleTypeDef *encoder, uint16_t pwmFrequency); // CONF: OUTS, PWMF
HAL_StatusTypeDef AS5200L_setSlowFilter(AS5200L_HandleTypeDef *encoder, uint8_t mode);         // CONF: SF
HAL_StatusTypeDef AS5200L_setFastFilter(AS5200L_HandleTypeDef *encoder, uint8_t mode);         // CONF: FTH
HAL_StatusTypeDef AS5200L_setLPMTimer(AS5200L_HandleTypeDef *encoder, uint8_t value);          // CONF: ALPM

// Status registers
HAL_StatusTypeDef AS5200L_getMagnetStatus(AS5200L_HandleTypeDef *encoder, uint8_t *status); // STATUS: MD, ML, MH
HAL_StatusTypeDef AS5200L_getGain(AS5200L_HandleTypeDef *encoder, uint8_t *gain);         // STATUS: AGC
HAL_StatusTypeDef AS5200L_getMagnitude(AS5200L_HandleTypeDef *encoder, uint16_t *magnitude);   // MAGNITUDE: MAGNITUDE
HAL_StatusTypeDef AS5200L_isSensorOverflowed(AS5200L_HandleTypeDef *encoder, uint8_t *isOverflowed); // STATUS: ADCOF

// Burn commands
HAL_StatusTypeDef AS5200L_burnAngle(AS5200L_HandleTypeDef *encoder);
HAL_StatusTypeDef AS5200L_burnSettings(AS5200L_HandleTypeDef *encoder);

#endif
